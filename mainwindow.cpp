#include "mainwindow.h"
#include "ui_form.h"
#include <QDebug>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent),ui(new Ui::Form)
{
//    ui->setupUi(this);
    this->resize(QSize(1140,150));

    Widget = new QWidget(this);
    Widget->setEnabled(true);

    VLayout1 = new QVBoxLayout(Widget);

    HLayout1 = new QHBoxLayout();

    GroupRadioButtons = new QButtonGroup();

    RadioButton1 = new QRadioButton(QString("Radio 1"));
    RadioButton1->setChecked(true);
    RadioButton2 = new QRadioButton(QString("Radio 2"));
    RadioButton3 = new QRadioButton(QString("Radio 3"));

    GroupRadioButtons->addButton(RadioButton1,0);
    GroupRadioButtons->setId(RadioButton1,0);

    GroupRadioButtons->addButton(RadioButton2,1);
    GroupRadioButtons->setId(RadioButton2,1);

    GroupRadioButtons->addButton(RadioButton3,2);
    GroupRadioButtons->setId(RadioButton3,2);

    HLayout2 = new QHBoxLayout();
    GroupBox1 = new QGroupBox(QString("Grupa 1"));
    GroupBox1->setStyleSheet("QGroupBox{font-weight: bold; border: 1px solid gray; border-radius: 5px; margin-top: 1ex;} QGroupBox::title{subcontrol-origin: margin; subcontrol-position: top center; padding: 0 3px;}");
    GroupBox1->setEnabled(true);
    GroupBox2 = new QGroupBox(QString("Grupa 2"));
    GroupBox2->setStyleSheet("QGroupBox{font-weight: bold; border: 1px solid gray; border-radius: 5px; margin-top: 1ex;} QGroupBox::title{subcontrol-origin: margin; subcontrol-position: top center; padding: 0 3px;}");
    GroupBox2->setEnabled(false);
    GroupBox3 = new QGroupBox(QString("Grupa 3"));
    GroupBox3->setStyleSheet("QGroupBox{font-weight: bold; border: 1px solid gray; border-radius: 5px; margin-top: 1ex;} QGroupBox::title{subcontrol-origin: margin; subcontrol-position: top center; padding: 0 3px;}");
    GroupBox3->setEnabled(false);
    /*****/
    VLayout2 = new QVBoxLayout(GroupBox1);
    HLayout3 = new QHBoxLayout();
    Label1 = new QLabel(QString("Label1"));
//    Label1->setEnabled(false);
//    L1 = Label1->isEnabled();

    Label2 = new QLabel(QString("Label2"));
//    Label2->setEnabled(false);
//    L2 = Label2->isEnabled();

    Label3 = new QLabel(QString("Label3"));
//    Label3->setEnabled(false);
//    L3 = Label3->isEnabled();

    HLayout4 = new QHBoxLayout();
    DateEdit1 = new QDateEdit();
//    DateEdit1->setEnabled(false);
    DateEdit1->setCalendarPopup(true);
    DateEdit1->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Fixed);

    LineEdit1 = new QLineEdit();
//    LineEdit1->setEnabled(false);
    LineEdit1->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Fixed);

    ComboBox1 = new QComboBox();
//    ComboBox1->setEnabled(false);

    CheckBox1 = new QCheckBox(QString("CheckBox1"));
//    CheckBox1->setEnabled(false);
    /*****/
    VLayout3 = new QVBoxLayout(GroupBox2);
    HLayout5 = new QHBoxLayout();
    Label4 = new QLabel(QString("Label4"));
    Label5 = new QLabel(QString("Label5"));
    HLayout6 = new QHBoxLayout();
    DateEdit2 = new QDateEdit();
    DateEdit2->setCalendarPopup(true);
    DateEdit2->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Fixed);
    DateEdit3 = new QDateEdit();
    DateEdit3->setCalendarPopup(true);
    DateEdit3->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Fixed);

    VLayout4 = new QVBoxLayout(GroupBox3);
    HLayout7 = new QHBoxLayout();
    Label6 = new QLabel(QString("Label6"));
    Label7 = new QLabel(QString("Label7"));
    HLayout8 = new QHBoxLayout();
    DateEdit4 = new QDateEdit();
    DateEdit4->setCalendarPopup(true);
    DateEdit4->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Fixed);
    DateEdit5 = new QDateEdit();
    DateEdit5->setCalendarPopup(true);
    DateEdit5->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Fixed);

    QObject::connect(RadioButton1, &QRadioButton::toggled, GroupBox1, &QWidget::setEnabled);    
    QObject::connect(RadioButton2, &QRadioButton::toggled, GroupBox2, &QWidget::setEnabled);
    QObject::connect(RadioButton3, &QRadioButton::toggled, GroupBox3, &QWidget::setEnabled);

    QObject::connect(GroupRadioButtons, SIGNAL(buttonClicked(int)), this, SLOT(setActiveRadioButtons(int)));

    HLayout9 = new QHBoxLayout();
    PushButton1 = new QPushButton(QString("Button1"));
    PushButton2 = new QPushButton(QString("Button2"));

    Widget->setLayout(VLayout1);
    VLayout1->addLayout(HLayout1);
    HLayout1->addWidget(RadioButton1);
    HLayout1->addWidget(RadioButton2);
    HLayout1->addWidget(RadioButton3);

    VLayout1->addLayout(HLayout2);
    HLayout2->addWidget(GroupBox1);
    HLayout2->addWidget(GroupBox2);
    HLayout2->addWidget(GroupBox3);

    VLayout2->addLayout(HLayout3);
    HLayout3->addWidget(Label1);
    HLayout3->addWidget(Label2);
    HLayout3->addWidget(Label3);

    VLayout2->addLayout(HLayout4);
    HLayout4->addWidget(DateEdit1);
    HLayout4->addWidget(LineEdit1);
    HLayout4->addWidget(ComboBox1);
    VLayout2->addWidget(CheckBox1);

    VLayout3->addLayout(HLayout5);
    HLayout5->addWidget(Label4);
    HLayout5->addWidget(Label5);
    VLayout3->addLayout(HLayout6);
    HLayout6->addWidget(DateEdit2);
    HLayout6->addWidget(DateEdit3);

    VLayout4->addLayout(HLayout7);
    HLayout7->addWidget(Label6);
    HLayout7->addWidget(Label7);
    VLayout4->addLayout(HLayout8);
    HLayout8->addWidget(DateEdit4);
    HLayout8->addWidget(DateEdit5);

    VLayout1->addLayout(HLayout9);
    HLayout9->addWidget(PushButton1);
    HLayout9->addWidget(PushButton2);

    setCentralWidget(Widget);
}

MainWindow::~MainWindow()
{
}


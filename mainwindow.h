#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QWidget>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QRadioButton>
#include <QGroupBox>
#include <QButtonGroup>
#include <QLabel>
#include <QDateEdit>
#include <QLineEdit>
#include <QComboBox>
#include <QCheckBox>
#include <QPushButton>

#include <QDebug>

namespace Ui {
class Form;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    bool L1, L2, L3, L4, L5, L6, L7, DE1, DE2, DE3, DE4, DE5, LE1, CB1, ChB1;

    QWidget *Widget;
    QHBoxLayout *HLayout1, *HLayout2, *HLayout3, *HLayout4, *HLayout5, *HLayout6, *HLayout7, *HLayout8, *HLayout9;
    QVBoxLayout *VLayout1, *VLayout2, *VLayout3, *VLayout4, *VLayout5;

    QButtonGroup *GroupRadioButtons;
    QRadioButton *RadioButton1, *RadioButton2, *RadioButton3;
    QGroupBox *GroupBox1, *GroupBox2, *GroupBox3;
    QLabel *Label1, *Label2, *Label3, *Label4, *Label5, *Label6, *Label7;
    QDateEdit *DateEdit1, *DateEdit2, *DateEdit3, *DateEdit4, *DateEdit5;
    QLineEdit *LineEdit1;
    QComboBox *ComboBox1;
    QCheckBox *CheckBox1;

    QPushButton *PushButton1, *PushButton2;

//    bool activeWidgetsUnderRadioButton1();

    Ui::Form *ui;

private slots:
//    void setActiveWidgetsUnderRadioButton1(bool active);
    inline void setActiveRadioButtons(int active){
        if(active==0){
            qDebug()<<"Radio 1";
        }
        else if(active==1){
            qDebug()<<"Radio 2";
        }
        else if(active==2){
            qDebug()<<"Radio 3";
        }
    }

signals:
};
#endif // MAINWINDOW_H
